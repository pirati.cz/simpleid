��    *      l  ;   �      �     �  :   �  	   �     �       	                  ,     3     K     W     ^     e     w  &        �  f   �  /      
   P     [     d  	   i  +   s     �  *   �  D   �               "     +  
   @     K     Q     d     k  @   s  &   �     �      �  !     �  8     �  ?   �     	      	     '	     3	     A	     J	     V	     ^	     x	     �	     �	     �	     �	  2   �	     
  9   
  7   W
     �
     �
     �
     �
  /   �
  	   �
  >   �
  7   &     ^     g     o     |     �     �     �     �     �  D   �  (        G  "   \          #   )       (   &      "              	          *                                              !                                     '              $                   
                       %           Access Denied An encrypted connection (HTTPS) is required for this page. Automatic Cancel Continue Dashboard Disable Documentation Enable Enter Verification Code Last access Log In Log in Log in cancelled. Log out Log out and log in as a different user Login Verification Login verification is <strong>disabled</strong>. To enable login verification, click the button below. Login verification is <strong>enabled</strong>. My Profile My Sites Name Password: Remember me on this computer for two weeks. Remove Secure login using <strong>HTTPS</strong>. SimpleID will also be sending the following information to the site. Site Submit Type URL User %uid not found. User name: Value Verification code: Verify Welcome You are being logged into <strong class="realm">@realm</strong>. You are logged in as %uid (%identity). You have been logged out. You were logged in successfully. Your preferences have been saved. Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-01 14:15+0100
PO-Revision-Date: 2016-03-01 18:05+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.6
Last-Translator: 
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
Language: cs_CZ
X-Poedit-SourceCharset: UTF-8
 Přístup odepřen Pro tuto stránku je vyžadováno šifrované spojení (HTTPS). Automatické Storno Pokračovat Hlavní panel Zakázat Dokumentace Povolit Vložte ověřovací kód Poslední přístup Přihlásit se Přihlásit se Přihlášení zrušeno. Odhlásit se Odhlásit se a přihlásit se jako jiný uživatel Ověření přihlášení Ověření přihlášení je <strong>zakázáno</strong>. Ověření přihlášení je <strong>povoleno</strong>. Profil Aplikace Jméno Heslo: Zapamatovat na tomto počítači po dva týdny. Odstranit Bezpečné přihlášení s využitím <strong>HTTPS</Strong>. SimpleID také zašle stránce následující inforace. Stránka Odeslat Napište URL Uživatel %uid nenalezen. Uživatelské jméno: Hodnota Ověřovací kód: Ověřit Vítejte Přihlašujete se do aplikace <strong class="realm">@realm</strong>. Jste přihlášen jako %uid (%identity). Byl jste odhlášen. Byl jste úspěšně přihlášen. Vaše nastavení byla uložena. 